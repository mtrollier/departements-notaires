function styleIdent(variable,texte)
{
	document.getElementById(variable).value = texte;
	document.getElementById(variable).style.fontStyle = 'italic';
	document.getElementById(variable).style.color = '#9e9e9e';
}
function viderLogin(contenu)
{
	document.getElementById(contenu).value = '';
	document.getElementById(contenu).style.color = '#000000';
	document.getElementById(contenu).style.fontStyle = 'normal';
}

function check(casecoche,visible )
{
	if (document.getElementById(casecoche).checked==false)
	{
		document.getElementById(visible).style.display = "none";
		document.getElementById(visible).style.visibility = "hidden";
	}
	else
	{
		document.getElementById(visible).style.display = "block";
		document.getElementById(visible).style.visibility = "visible";
	}
}

function vider(contenu)
{
	document.getElementById(contenu).value = '';
}

function codeTouche(evenement)
{
    for (prop in evenement)
    {
            if(prop == 'which') return(evenement.which);
    }
    return(evenement.keyCode);
}

function scanTouchelettre(evenement)
{
    var reCarValides = /[a-zA-Z]/;
	var reCarSpeciaux = /[\x00\x08\x0D\x2D\x20\x27]/; //nul Backspace \r - espace '
    var codeDecimal  = codeTouche(evenement);
    var car = String.fromCharCode(codeDecimal);
    var autorisation = reCarValides.test(car) || reCarSpeciaux.test(car);

    return autorisation;
}

function scanTouchelettre_pren(evenement)
{
    var reCarValides = /[a-zA-Z]/;
	var reCarSpeciaux = /[\x00\x08\x0D\x2D\x20\xE8\xE9\xF4\xF6\xEF\xEB\xE4\xE7]/; //nul Backspace \r - espace è é ô ö ï ê ä ç
    var codeDecimal  = codeTouche(evenement);
    var car = String.fromCharCode(codeDecimal);
    var autorisation = reCarValides.test(car) || reCarSpeciaux.test(car);

    return autorisation;
}

function scanTouchelettre_pren_plus(evenement)
{
    var reCarValides = /[a-zA-Z]/;
	var reCarSpeciaux = /[\x00\x08\x0D\x2D\x20\xE8\xE9\xF4\xF6\xEF\xEB\xE4\xE7\x2C]/; //nul Backspace \r - espace è é ô ö ï ê ä ç ,
    var codeDecimal  = codeTouche(evenement);
    var car = String.fromCharCode(codeDecimal);
    var autorisation = reCarValides.test(car) || reCarSpeciaux.test(car);

    return autorisation;
}
function scanTouchechiffre(evenement)
{
    var reCarSpeciaux = /[\x00\x08\x0D]/; //nul Backspace \r
    var reCarValides = /\d/;

    var codeDecimal  = codeTouche(evenement);
    var car = String.fromCharCode(codeDecimal);
    var autorisation = reCarValides.test(car) || reCarSpeciaux.test(car);

    return autorisation;
}


function add_slashes(date)
{
	// JJMMAAAA
	test = document.getElementById(date).value;
	//alert (test);
	var jour = test.substring(0,2);
	var mois = test.substring(2,4);
	var annee = test.substring(4,8);
	var date_finale = jour +"/"+ mois + "/" + annee;
	if (date_finale == '')
	{document.getElementById(date).value = '';}
	if (date_finale == '//')
	{document.getElementById(date).value = '';}
	else
	{document.getElementById(date).value = date_finale;}

}


function visible(visi)
{
    if (document.getElementById(visi).style.display == "none")
    {
        document.getElementById(visi).style.display = "table-row";
        document.getElementById("prenomP").text = '-';
    }
    else
    {
        document.getElementById(visi).style.display = "none";
        document.getElementById("prenomP").text ='+';
    }
}

function  verifForm_Recherche()
{
    if(loginform.date_acte.value == '' && loginform.date_deces.value == '' &&  loginform.lieu_deces.value == '' &&  
            loginform.prenom.value == '' &&  loginform.nom_usage.value == ''&&loginform.date_naissance == '') 
    {
          return false;
    }
}


//Fonction de formatage des dates, ajout des slashes et validation de la validit� du mois du jour et de l'année. 
function add_slashes_Recherche(date)
{
    var now = new Date();
    var anneencours = now.getFullYear();
    
    // JJMMAAAA
    test = document.getElementById(date).value;
    //alert (test);
    var jour = test.substring(0,2);
    var mois = test.substring(2,4);
    var annee = test.substring(4,8);

    var date_finale = jour +"/"+ mois + "/" + annee;
    // Initialisation des champs
    if (date_finale ==  "")
    {
        document.getElementById(date).value = '';
    }
    //Si le champs contient les slashes on le vide
    else if (date_finale == '//')
    {
        document.getElementById(date).value = '';
    }
    // Si le mois égal 00 ou est superieur a 12 on vide le champ et on indique à l'utilisateur que la date est invalide.
    else if (mois <=00 || mois >= 13)
    {
        document.getElementById(date).value ='';
        document.getElementById('msg_erreur').value ='Format du mois invalide.';
        document.getElementById('msg_erreur').style.display ='inline';
    }
    // Si le jour égal 00 ou est superieur a 31 on vide le champ et on indique à l'utilisateur que la date est invalide.
    else if (jour >=32 || jour <= 00)
    {
        document.getElementById(date).value ='';
        document.getElementById('msg_erreur').value ='Format du jour invalide.';
        document.getElementById('msg_erreur').style.display ='inline';
    }
    // Si l'année est inf�ieure ou égale a 1899  ou est superieur a l'année en cours plus 1 on vide le champ et on indique à l'utilisateur que la date est invalide.
    else if (annee <=1899 || annee >=anneencours+1)
    {
        document.getElementById(date).value ='';
        document.getElementById('msg_erreur').value ='Format de l\'année invalide.';
        document.getElementById('msg_erreur').style.display ='inline';
    }
    // Sinon on met a jour le champs avec les slashes et on n'affiche plus le div de message d'erreur.
    else
    {
        document.getElementById(date).value = date_finale;
        document.getElementById('msg_erreur').style.display ='none';
    }
}

// Fonction de comparaison des dates de décès et date de l'acte de décès.
function compare_Date_acte_deces_naissance()
{
    // Récupération  de la date du champ date de décès.
    var datedeces = document.getElementById('date_deces').value;
    var date1 = new Date(datedeces.substr(6,8),datedeces.substr(3,2)-1,datedeces.substr(0,2),0,0,0);
    d1 = date1.getTime();
    
    // Récupération  de la date du champ date de l'acte.
    var dateacte = document.getElementById('date_acte').value;
    var date2 = new Date(dateacte.substr(6,8),dateacte.substr(3,2)-1,dateacte.substr(0,2),0,0,0);
    d2 = date2.getTime();
    
    // Récupération  de la date du champ date de naissance.
    var datenaiss = document.getElementById('date_naissance').value;
    var date3 = new Date(datenaiss.substr(6,8),datenaiss.substr(3,2)-1,datenaiss.substr(0,2),0,0,0);
    d3 = date3.getTime();
    // Récupération  de la date du jour.
    var actuelle = new Date();
    var test = new Date(actuelle.getFullYear(),actuelle.getMonth(),actuelle.getDate(),0,0,0);
    actu = test.getTime();
    // Si la date de décès est plus récente que la date du jour on vide le champ et on indique qu'il y a une erreur.
     if (actu <= d1)
    {
        document.getElementById('date_deces').value ='';
        document.getElementById('msg_erreur').value ='La date de décès ne peut pas être postérieure à la date du jour.';
        document.getElementById('msg_erreur').style.display ='inline';
    }
    // Si la date de l'acte est plus récente que la date du jour on vide le champ et on indique qu'il y a une erreur.
     else if (actu <= d2)
    {
        document.getElementById('date_acte').value ='';
        document.getElementById('msg_erreur').value ='La date de l\'acte ne peut pas être postérieure que la date du jour.';
        document.getElementById('msg_erreur').style.display ='inline';
    }
    // Si la date de naissance est plus récente que la date du jour on vide le champ et on indique qu'il y a une erreur.
    else if (actu <= d3)
    {
        document.getElementById('date_naissance').value ='';
        document.getElementById('msg_erreur').value ='La date de naissance ne peut pas être postérieure à la date du jour.';
        document.getElementById('msg_erreur').style.display ='inline';
    }
    else if (d1 > d2 && dateacte  !=  ''  && datedeces != '')
    {
        document.getElementById('date_acte').value ='';
        document.getElementById('msg_erreur').value ='La date de décès ne peut être postérieure à la date de l\'acte.';
        document.getElementById('msg_erreur').style.display ='inline';
    }
}

/* Initialisation XMLHttpRequest */
function getXhr(){
    var xhr = null; 
    if(window.XMLHttpRequest) // Firefox et autres
        xhr = new XMLHttpRequest(); 
    else if(window.ActiveXObject){ // Internet Explorer 
        try {
            xhr = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    else { // XMLHttpRequest non supporté par le navigateur 
        alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
        xhr = false; 
    } 
    return xhr;
}

/**
 * Méthode qui affiche les mois lors du changement de l'année
 */
function Afficher_mois_Stats(id_source,id_cible){
    var xhr = getXhr();
    // On défini ce qu'on va faire quand on aura la réponse
    xhr.onreadystatechange = function(){
        // On ne fait quelque chose que si on a tout reçu et que le serveur est ok
        if(xhr.readyState == 4 && xhr.status == 200){
            leselect = xhr.responseText;
            // On se sert de innerHTML pour rajouter les options a la liste
            document.getElementById(id_cible).innerHTML = leselect;
        }
    }

    // Ici on va voir comment faire du post
    xhr.open("POST","mois_stats.php",true);
    // ne pas oublier ça pour le post
    xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
    // ne pas oublier de poster les arguments
    sel = document.getElementById(id_source);
    valeur = sel.options[sel.selectedIndex].value;
    xhr.send("valeur="+valeur+"nomid="+id_cible);
}


//Fonction de comparaison des dates de début et date de fin des statistiques.
function compare_Date(day_start,month_start,year_start,day_end,month_end,year_end)
{
    var message="";
    valid=true;
   
    var jour_debut=1;
    var jour_fin=1;
    
    // Récupération  de la date du champ date de début.
    var mois_debut=document.getElementById(month_start).value;
    var annee_debut=document.getElementById(year_start).value;
    
    // Récupération  de la date du champ date de fin.
    var mois_fin=document.getElementById(month_end).value;
    var annee_fin=document.getElementById(year_end).value;
    
    if(day_start!=""){
        jour_debut=document.getElementById(day_start).value;
    }
    
    if(day_end!=""){
        jour_fin=document.getElementById(day_end).value;
    }
    
    
    if(annee_debut==""){
        message+="Sélectionner un année de début\n";
        valid=false;
    }
    if(mois_debut==""){
        message+="Sélectionner un mois de début\n";
        valid=false;
    }
    if(annee_fin==""){
        message+="Sélectionner un année de fin\n";
        valid=false;
    }
    if(mois_fin==""){
        message+="Sélectionner un mois de fin\n";
        valid=false;
    }
    
    var date1 = new Date(annee_debut,mois_debut-1,jour_debut,0,0,0);
    d1 = date1.getTime();

    var date2 = new Date(annee_fin,mois_fin-1,jour_fin,0,0,0);
    d2 = date2.getTime();

    if (d2 < d1)
    {
        message+="La date de fin doit être inférieure à la date de début\n";
        valid=false;
    }
    
    if(valid==false){
        alert(message);
    }
    return valid;
}