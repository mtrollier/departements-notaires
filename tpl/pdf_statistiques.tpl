{# 
Template pour la génération des statistiques dans un fichier PDF.
En plus des variables utilisables dans tous les templates (config.xxx), on peut utiliser :
- graph_path    => chemin du graphique généré en amont
- series        => le tableau des series "mois", "demandes", "recuperables", "indus", "inconnus", "ambigus", "connexions"
  "utilisateurs"
- dateTo        => date début de la période
- dateFrom      => date fin de la période  
#}

<style>
    @page { margin: 100px 25px; }
    header { 
        position: fixed; 
        top: -80px; 
        left: 0px; 
        right: 0px; 
        height: 50px; 
        font-weight: bold;
        font-size: 18pt;
        color: white;
        
    }
    footer { 
        position: fixed; 
        bottom: -80px; 
        left: 0px; 
        right: 0px; 
        height: 20px; 
        font-style: italic;
        font-size: 11px;
        color: grey;
    }
    footer .pagenum:before {
          content: counter(page);
    }    
    p { page-break-after: always; }
    p:last-child { page-break-after: never; }
    
    .pagenum-container {
        float:right;
    }
    table {
        border-collapse:collapse;
        width: 100%;
    } 
    td, th {
        border: 1px solid #ddd;
        padding: 8px;
    }    
    tr:nth-child(odd){
        background-color: rgb(224,235,255);
    }
    th {
        background-color:rgb(0,102,178);
        font-weight: bold;
        font-size: 10pt;
        color : white;
        text-align: center;
    }
    td {
        font-size: 10pt;
        color : black;
        text-align: center;
    }
    
</style>

{# Entête de page #}
<header>
    {# Logo département #}
    <div style="float:left">
        <img src=".private/logo_.png" />
    </div>    
    {# Titre du document #}
    <div style="float:right;background-color:rgb(0,102,178);padding:10px;margin-right:25px;margin-top:20px;width:80%;text-align:center;">
        Statistiques d'utilisation de {{config.nom_application}}
    </div>
</header>

{# Pied de page #}
<footer>
    {# Pied de page #}
    Date d'impression {{ "now"|date("d/m/Y") }}  
    <div class="pagenum-container">Page <span class="pagenum"></span></div>
</footer>

{# Tableau #}
<table>
  <thead>
    <th>Mois</th>
    <th>Nb recherches</th>
    <th>Nb réponses totales</th>
    <th>Nb réponses récupérables</th>
    <th>Nb réponses indus probables</th>
    <th>Nb réponses cas ambigus</th>
    <th>Nb réponses individus inconnus</th>
    <th>Nb connexions</th>
    <th>Nb d'utilisateurs différents</th>
  </thead>
  <tbody>
  {% set maxMois = series['mois']|length - 1 %}
  {% for i in 0..maxMois %}
  <tr>
    <td style="white-space:nowrap;font-weight:bold">{{ series['mois'][i] }}</td>
    <td>{{ series['demandes'][i] }}</td>
    <td>{{ series['demandes'][i] }}</td>
    <td>{{ series['recuperables'][i] }}</td>
    <td>{{ series['indus'][i] }}</td>
    <td>{{ series['ambigus'][i] }}</td>
    <td>{{ series['inconnus'][i] }}</td>
    <td>{{ series['connexions'][i] }}</td>
    <td>{{ series['utilisateurs'][i] }}</td>
  </tr>
  {% endfor %}
  </tbody>
</table>
{# Saut de page avant le graphique #}
<div style="page-break-after: always;">&nbsp;</div>

{# Graphique #}
<br /><br />
<img src="{{graph_path}}" />



