{# Formulaire de saisie nouveau mot de passe #}

<div class="error_msg">
  {{error_msg}}
</div>

<div id="cadre_reinit">
  <form action="index.php?index=modifier_mdp" method="post">
    <table>
      <tr>
        <td colspan="2">
          <p id="message_reinit">
            Votre nouveau mot de passe doit être différent du précédent. Il doit contenir
            8 caractères avec au moins une majuscule, un chiffre et un caractère spécial (?/!*%$).
          </p>
        </td>
      </tr>      
      <tr>
          <td><p class="message_reinit">Ancien Mot de Passe :</p></td>
          <td><input type="password" id="pass_old" name="pass_old" required="required"></td>
      </tr>
      <tr>
          <td><p class=message_reinit>Nouveau Mot de Passe :</p></td>
          <td><input type="password" id="pass" name="pass" required="required"></td>
      </tr>
      <tr>
          <td><p class="message_reinit">Confirmer :</p></td>
          <td><input type="password" id="pass_confirm"  name="pass_confirm" required="required"></td>
      </tr>
    </table>
    <input type="submit" id="submit_reinit" />
  </form>
</div>


<div class="info_msg">
  {{message}}
</div>