﻿{# Formulaire de consultation des satistiques 
En plus des variables utilisables dans tous les templates (config.xxx), on peut utiliser :
- years    : tableau des années possibles
- series   : le tableau des series "mois", "demandes", "recuperables", "indus", "inconnus", "ambigus", "connexions"
#}

{# Export du fichier PDF #}
<div id="recherche_listing">
  <form name="selecto" method="post" action="stats.php"
    onsubmit="return compare_Date('','month_start','year_start','','month_end','year_end');">
    
    <div id="listing_date_deb">
      <label for="day_start">Date de début : </label> 
      <select id="year_start" name="year_start"  onchange="Afficher_mois_Stats('year_start','month_start')" />
        {% for year in years %}
        <option value='{{year}}' selected="true">{{year}}</option>
        {% endfor %}
      </select> 
      <select id="month_start" name="month_start" >
      </select>
      
      <label for="day_end">Date de fin : </label>
      <select id="year_end" name="year_end" onchange="Afficher_mois_Stats('year_end','month_end')" />
        {% for year in years %}
        <option value='{{year}}' selected="true">{{year}}</option>
        {% endfor %}
      </select>
      <select id="month_end" name="month_end" /> 
      </select>

      <input type="submit" id="submit_listing" name="submit_listing" value="Générer un PDF" />
    </div>
  </form>
</div>
<script>
Afficher_mois_Stats('year_start','month_start');
Afficher_mois_Stats('year_end','month_end');
</script>

<style>
td.center {
  text-align: center;
}
td.boldcenter {
  text-align: center;
  font-weight: bold;
}
</style>

{# Tableau des statistiques des 5 derniers mois #}
<table class="table_stats" width="100%">
  <thead>
    <th>&nbsp;</th>
    {% for mois in series['mois'] %}
    <th width="15%">{{mois}}</th>
    {% endfor %}
  </thead>
  <tbody>
    <tr class="stats">
      <td>Nombre de demandes</td>
      {% set bold=true %}
      {% for nombre in series['demandes'] %}
      <td class="{% if bold %}bold{%endif%}center">{{nombre}}</td>
      {% set bold=false %}
      {% endfor %}      
    </tr>
    <tr class="stats">
      <td>Nombre de réponses :</td>{# identique au nombre de demandes #}
      {% set bold=true %}
      {% for nombre in series['demandes'] %}
      <td class="{% if bold %}bold{%endif%}center">{{nombre}}</td>
      {% set bold=false %}
      {% endfor %}      
    </tr>
    <tr class="stats">
      <td>- Récupérables</td>
      {% set bold=true %}
      {% for nombre in series['recuperables'] %}
      <td class="{% if bold %}bold{%endif%}center">{{nombre}}</td>
      {% set bold=false %}
      {% endfor %}      
    </tr>
    <tr class="stats">
      <td>- Indus Probables</td>
      {% set bold=true %}
      {% for nombre in series['indus'] %}
      <td class="{% if bold %}bold{%endif%}center">{{nombre}}</td>
      {% set bold=false %}
      {% endfor %}      
    </tr>
    <tr class="stats">
      <td>- Cas ambigus</td>
      {% set bold=true %}
      {% for nombre in series['ambigus'] %}
      <td class="{% if bold %}bold{%endif%}center">{{nombre}}</td>
      {% set bold=false %}
      {% endfor %}      
    </tr>
    <tr class="stats">
      <td>- Individus Inconnus</td>
      {% set bold=true %}
      {% for nombre in series['inconnus'] %}
      <td class="{% if bold %}bold{%endif%}center">{{nombre}}</td>
      {% set bold=false %}
      {% endfor %}      
    </tr>
    <tr class="stats">
      <td>Nombre de connexions</td>{# identique au nombre de demandes #}
      {% set bold=true %}
      {% for nombre in series['connexions'] %}
      <td class="{% if bold %}bold{%endif%}center">{{nombre}}</td>
      {% set bold=false %}
      {% endfor %}      
    </tr>
    <tr class="stats">
      <td>Nombre d'utilisateurs différents</td>{# identique au nombre de demandes #}
      {% set bold=true %}
      {% for nombre in series['utilisateurs'] %}
      <td class="{% if bold %}bold{%endif%}center">{{nombre}}</td>
      {% set bold=false %}
      {% endfor %}      
    </tr>
  </tbody>
</table>


{# Graphique des statistiques des 5 derniers mois #}
<br/><br/>
<img src="stats_graphique.php"/>