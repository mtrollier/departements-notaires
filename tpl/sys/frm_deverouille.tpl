{# Formulaire déverrouillage du compte #}

<div id="message_reponse">
  Un message électronique vous a été envoyé sur votre adresse de messagerie.<br />
  Merci de rentrer le code qui vous a été  fourni dans ce message, afin de déverrouiller votre compte.
  <br/>
  <br/>
  <b>ATTENTION</b> : si vous fermez cette fenêtre, le code reçu ne sera plus valide.
  <br/>
  <br/>
  
  <form action="index.php?index=deverouille" method="post">
    <label for="dever">Code reçu</label>
    <input type=text name=dever id=dever onclick="viderLogin('dever')">
    <input id="submit_deverouille" type="submit">
  </form>
</div>

